## Setup and configuration

Requirements:

* PHP 7.0 for demo
* Composer and PHPUnit for unit tests

Installing Composer:

``` bash
curl -s https://getcomposer.org/installer | php
sudo mv composer.phar /usr/local/bin/composer
```

App setup:

``` bash
ssh://git@bitbucket.org/amiruaro/d3m0s.git demo
cd demo
composer install
php -S 0.0.0.0:8080 -t public/
```

App is accessible using http://54.169.116.154:8080

Database setup:

http://54.169.116.154:8080/bootstrap.php

Unit test:

``` bash
cd demo
cd vendor/bin/phpunit -c phpunit.xml
```

## App usage

App usage is per the instructons (instructions.md).

For DELETE requests:

* /campaigns/[id]
* /ad_groups/[id]
* /expanded_text_ads/[id]