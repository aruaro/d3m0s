<?php
// entry point of the app

// enable class autoloader
require_once __DIR__.'/../src/autoload.php';

error_reporting(0);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
ini_set('memory_limit', '256M');
ini_set('max_execution_time', '60');
ini_set('opcache.enable', false);

define('STORAGE_PATH', __DIR__ . '/../storage');
define('APP_PATH', __DIR__ . '/../src/app');
define('APP_ENV', 'prod');

$config = [
    'database' => [
        'driver' => 'sqlite',
        'path'   => STORAGE_PATH . '/data.sqlite3'
    ],
    'cache' => [
        'driver'  => 'jsonfile',
        'enabled' => true,
        'expires' => 10,
        'path'    => STORAGE_PATH . '/cache'
    ]
];

// load routes
$routes = require_once __DIR__.'/../src/app/routes.php';

// load services
$services = [
    'cache'    => 'Lib\\Cache\\CacheServiceProvider',
    'database' => 'Lib\\Database\\DatabaseServiceProvider'
];

new Lib\Application($config, $routes, $services);
