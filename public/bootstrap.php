<?php

error_reporting(E_ALL);
ini_set('display_errors', true);
ini_set('display_startup_errors', true);
ini_set('max_execution_time', '60');
ini_set('opcache.enable', false);

define('STORAGE_PATH', __DIR__ . '/../storage');

class SampleData
{
    public static function generate($filter = null)
    {
        $texts = file_get_contents(__DIR__ . '/../storage/cheese.json');
        $texts = json_decode($texts, true);

        $campaigns = [];

        for ($i = 1; $i <= 5; $i++) {
            $campaigns[] = [
                // 'id'                       => $i,
                'name'                     => $texts['name'][mt_rand(0, 10)],
                'status'                   => ['PAUSED', 'ON'][mt_rand(0, 1)],
                'budget'                   => mt_rand(1, 15) * 100,
                'advertising_channel_type' => ['SEARCH', 'DISPLAY'][mt_rand(0, 1)]
            ];
        }

        $adGroups = [];

        for ($i = 1; $i <= 5; $i++) {
            $adGroups[] = [
                // 'id'          => $i,
                'name'        => $texts['name'][mt_rand(0, 10)],
                'status'      => ['ENABLED', 'OFFLINE'][mt_rand(0, 1)],
                'campaign_id' => mt_rand(1, 5)
            ];
        }

        $textAds = [];

        for ($i = 1; $i <= 5; $i++) {
            $textAds[] = [
                // 'id'             => $i,
                'xsi_type'       => 'ExpandedTextAd',
                'ad_group_id'    => mt_rand(1, 5),
                'headline_part1' => $texts['short'][mt_rand(0, 6)],
                'headline_part2' => $texts['medium'][mt_rand(0, 5)],
                'description'    => $texts['long'][mt_rand(0,5)],
                'path1'          => 'all-inclusive',
                'path2'          => 'deals'
            ];
        }

        $data = [
            'campaigns' => $campaigns,
            'ad_groups' => $adGroups,
            'expanded_text_ads' => $textAds
        ];

        if($filter != null) {
            foreach ($data[$filter] as $d) {
                print json_encode($d) . "\n";
            }
        }

        return $data;
    }

    public static function bootstrap()
    {
        if (file_exists(STORAGE_PATH . '/data.sqlite3')) {
            unlink(STORAGE_PATH . '/data.sqlite3');
        }

        $db  = new PDO('sqlite:' . STORAGE_PATH . '/data.sqlite3');
        $sql = file_get_contents(STORAGE_PATH . '/data.sql');

        $db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $db->exec($sql);

        $data = self::generate();

        $sql  = 'INSERT INTO campaign (name, status, budget, advertising_channel_type) VALUES(?, ?, ?, ?);';
        $stmt = $db->prepare($sql);

        foreach ($data['campaigns'] as $d) {
            $i = 1;
            foreach ($d as $k => $v) {
                $stmt->bindValue($i++, $v, PDO::PARAM_STR);
            }

            $stmt->execute();
        }

        $sql = 'INSERT INTO ad_group (name, status, campaign_id) VALUES(?, ?, ?);';
        $stmt = $db->prepare($sql);

        foreach ($data['ad_groups'] as $d) {
            $i = 1;
            foreach ($d as $k => $v) {
                $stmt->bindValue($i++, $v, PDO::PARAM_STR);
            }

            $stmt->execute();
        }

        $sql = 'INSERT INTO expanded_text_ad (xsi_type, ad_group_id, headline_part1, headline_part2, description, path1, path2) VALUES(?, ?, ?, ?, ?, ?, ?);';
        $stmt = $db->prepare($sql);

        foreach ($data['expanded_text_ads'] as $d) {
            $i = 1;
            foreach ($d as $k => $v) {
                $stmt->bindValue($i++, $v, PDO::PARAM_STR);
            }

            $stmt->execute();
        }
    }
}

SampleData::bootstrap();

echo "bootstrapped\n";
