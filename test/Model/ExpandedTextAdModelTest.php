<?php

namespace Test\Model;

use App\Model\ExpandedTextAd;
use Test\Model\BaseModelTest;


class ExpandedTextAdModelTest extends BaseModelTest
{
    public function adGroupDataProvider()
    {
        $data   = [];
        $dataset = [
            '{"id":1,"xsi_type":"ExpandedTextAd","ad_group_id":2,"headline_part1":"Paneer mascarpone edam cheese and wine","headline_part2":"Roquefort blue castello cheesy feet rubber cheese bocconcini mascarpone cheese slices dolcelatte","description":"Cottage cheese boursin brie cheesy feet lancashire who moved my cheese monterey jack melted cheese.","path1":"all-inclusive","path2":"deals"}',
            '{"id":2,"xsi_type":"ExpandedTextAd","ad_group_id":5,"headline_part1":"Paneer mascarpone edam cheese and wine","headline_part2":"Fromage emmental rubber cheese danish fontina cheese strings babybel hard cheese danish fontina","description":"Cottage cheese boursin brie cheesy feet lancashire who moved my cheese monterey jack melted cheese.","path1":"all-inclusive","path2":"deals"}',
            '{"id":3,"xsi_type":"ExpandedTextAd","ad_group_id":1,"headline_part1":"Port-salut cheesy grin rubber cheese","headline_part2":"Cheese strings stilton gouda cow bavarian bergkase roquefort melted cheese ricotta","description":"Paneer parmesan cut the cheese monterey jack taleggio croque monsieur cottage cheese who moved my cheese","path1":"all-inclusive","path2":"deals"}',
        ];

        foreach ($dataset as $arg) {
            $data[] = [json_decode($arg, true)];
        }

        return $data;
    }

    public function testGetterSetter()
    {
        list($cache, $db) = $this->mockTestGetterSetter();

        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertNull($textAd->setId(1));
        $this->assertNull($textAd->setXsiType('ExpandedTextAd'));
        $this->assertNull($textAd->setHeadlinePart1('dslkfjsdf'));
        $this->assertNull($textAd->setHeadlinePart2('ldnaf'));
        $this->assertNull($textAd->setDescription('asdosaj'));
        $this->assertNull($textAd->setPath1('jsacnin'));
        $this->assertNull($textAd->setPath2('lksadla'));
        $this->assertNull($textAd->setAdGroupId(1));
        $this->assertEmpty($textAd->errors);

        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertFalse($textAd->setId(null));
        $this->assertFalse($textAd->setXsiType(null));
        $this->assertFalse($textAd->setHeadlinePart1(null));
        $this->assertFalse($textAd->setHeadlinePart2(null));
        $this->assertFalse($textAd->setDescription(null));
        $this->assertFalse($textAd->setPath1(null));
        $this->assertFalse($textAd->setPath2(null));
        $this->assertFalse($textAd->setAdGroupId(null));
        $this->assertNotEmpty($textAd->errors);
    }

    public function testSelectAll()
    {
        list($cache, $db) = $this->mockTestSelectAll($this->adGroupDataProvider());

        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertNotEmpty($textAd->select());
    }

    public function testSelect()
    {
        list($cache, $db) = $this->mockTestSelect($this->adGroupDataProvider()[0]);

        $textAd = new ExpandedTextAd($cache, $db);

        $textAd->setId(1);
        $this->assertNotEmpty($textAd->select());
    }

    /**
     * @dataProvider adGroupDataProvider
     */
    public function testInsert($data)
    {
        list($cache, $db) = $this->mockTestInsert();
        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertFalse($textAd->insert());
        $this->assertNotEmpty($textAd->errors);

        $textAd = new ExpandedTextAd($cache, $db);

        $textAd->setHeadlinePart1($data['headline_part1']);
        $textAd->setAdGroupId($data['ad_group_id']);

        $this->assertTrue($textAd->insert());
        $this->assertNotEmpty($textAd->getId());
        $this->assertEmpty($textAd->errors);
    }

    /**
     * @dataProvider adGroupDataProvider
     */
    public function testUpdate($data)
    {
        list($cache, $db) = $this->mockTestUpdate();

        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertFalse($textAd->update());
        $this->assertNotEmpty($textAd->errors);

        $textAd = new ExpandedTextAd($cache, $db);

        $textAd->setId($data['id']);

        $this->assertTrue($textAd->update());
        $this->assertEmpty($textAd->errors);
    }

    public function testDelete()
    {
        list($cache, $db) = $this->mockTestDelete();

        $textAd = new ExpandedTextAd($cache, $db);

        $this->assertFalse($textAd->delete());
        $this->assertNotEmpty($textAd->errors);

        $textAd = new ExpandedTextAd($cache, $db);

        $textAd->setId(1);

        $this->assertTrue($textAd->delete());
        $this->assertEmpty($textAd->errors);
    }
}
