<?php

namespace Test\Model;


class BaseModelTest extends \PHPUnit_Framework_TestCase
{
    protected function mockTestGetterSetter()
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->getMock();

        return [$cache, $db];
    }

    protected function mockTestSelectAll($data)
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->setMethods(['fetch', 'save'])
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->setMethods(['query'])
                        ->getMock();

        $cache->expects($this->any())
            ->method('fetch')
            ->with($this->isType('string'))
            ->willReturn(false);

        $db->expects($this->any())
            ->method('query')
            ->with($this->isType('string'), $this->anything())
            ->willReturn($data);

        return [$cache, $db];
    }

    protected function mockTestSelect($data)
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->setMethods(['fetch', 'save'])
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->setMethods(['query'])
                        ->getMock();

        $cache->expects($this->any())
            ->method('fetch')
            ->with($this->isType('string'))
            ->willReturn(false);

        $db->expects($this->any())
            ->method('query')
            ->with($this->isType('string'), $this->isType('array'))
            ->willReturn($data);

        return [$cache, $db];
    }

    protected function mockTestInsert()
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->setMethods(['query', 'getLastInsertId'])
                        ->getMock();

        $db->expects($this->any())
            ->method('query')
            ->with($this->isType('string'), $this->isType('array'))
            ->willReturn(true);

        $db->expects($this->any())
            ->method('getLastInsertId')
            ->willReturn(1);

        return [$cache, $db];
    }

    protected function mockTestUpdate()
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->setMethods(['delete'])
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->setMethods(['query'])
                        ->getMock();

        $db->expects($this->any())
            ->method('query')
            ->with($this->isType('string'), $this->isType('array'))
            ->willReturn(true);

        return [$cache, $db];
    }

    protected function mockTestDelete()
    {
        $cache = $this->getMockBuilder('Lib\\Cache\\Cache')
                        ->disableOriginalConstructor()
                        ->setMethods(['delete'])
                        ->getMock();

        $db = $this->getMockBuilder('Lib\\Database\\Connection')
                        ->disableOriginalConstructor()
                        ->setMethods(['query'])
                        ->getMock();

        $db->expects($this->any())
            ->method('query')
            ->with($this->isType('string'), $this->isType('array'))
            ->willReturn(true);

        return [$cache, $db];
    }
}
