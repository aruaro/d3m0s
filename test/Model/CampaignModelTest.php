<?php

namespace Test\Model;

use App\Model\Campaign;
use Test\Model\BaseModelTest;

class CampaignModelTest extends BaseModelTest
{
    public function campaignDataProvider()
    {
        $data   = [];
        $dataset = [
            '{"id":1,"name":"Cotija","status":"PAUSED","budget":1400,"advertising_channel_type":"SEARCH"}',
            '{"id":2,"name":"Roquefort","status":"ON","budget":800,"advertising_channel_type":"DISPLAY"}',
            '{"id":3,"name":"Cheddar","status":"ON","budget":1200,"advertising_channel_type":"DISPLAY"}'
        ];

        foreach ($dataset as $arg) {
            $data[] = [json_decode($arg, true)];
        }

        return $data;
    }

    public function testGetterSetter()
    {
        list($cache, $db) = $this->mockTestGetterSetter();

        $campaign = new Campaign($cache, $db);

        $this->assertNull($campaign->setId(1));
        $this->assertNull($campaign->setName('Testing testing'));
        $this->assertNull($campaign->setStatus('PAUSED'));
        $this->assertNull($campaign->setStatus('ON'));
        $this->assertNull($campaign->setBudget(200));
        $this->assertNull($campaign->setAdvertisingChannelType('SEARCH'));
        $this->assertNull($campaign->setAdvertisingChannelType('DISPLAY'));
        $this->assertEmpty($campaign->errors);

        $campaign = new Campaign($cache, $db);

        $this->assertFalse($campaign->setId(null));
        $this->assertFalse($campaign->setName(null));
        $this->assertFalse($campaign->setStatus(null));
        $this->assertFalse($campaign->setBudget(null));
        $this->assertFalse($campaign->setAdvertisingChannelType(null));
        $this->assertNotEmpty($campaign->errors);
    }

    public function testSelectAll()
    {
        list($cache, $db) = $this->mockTestSelectAll($this->campaignDataProvider()[0]);

        $campaign = new Campaign($cache, $db);

        $this->assertNotEmpty($campaign->select());
    }

    public function testSelect()
    {
        list($cache, $db) = $this->mockTestSelect($this->campaignDataProvider()[0]);

        $campaign = new Campaign($cache, $db);

        $campaign->setId(1);
        $this->assertNotEmpty($campaign->select());
    }

    /**
     * @dataProvider campaignDataProvider
     */
    public function testInsert($data)
    {
        list($cache, $db) = $this->mockTestInsert();

        $campaign = new Campaign($cache, $db);

        $this->assertFalse($campaign->insert());
        $this->assertNotEmpty($campaign->errors);

        $campaign = new Campaign($cache, $db);

        $campaign->setName($data['name']);
        $campaign->setStatus($data['status']);
        $campaign->setBudget($data['budget']);
        $campaign->setAdvertisingChannelType($data['advertising_channel_type']);

        $this->assertTrue($campaign->insert());
        $this->assertNotEmpty($campaign->getId());
        $this->assertEmpty($campaign->errors);
    }

    /**
     * @dataProvider campaignDataProvider
     */
    public function testUpdate($data)
    {
        list($cache, $db) = $this->mockTestUpdate();

        $campaign = new Campaign($cache, $db);

        $this->assertFalse($campaign->update());
        $this->assertNotEmpty($campaign->errors);

        $campaign = new Campaign($cache, $db);

        $campaign->setId($data['id']);

        $this->assertTrue($campaign->update());
        $this->assertEmpty($campaign->errors);
    }

    public function testDelete()
    {
        list($cache, $db) = $this->mockTestDelete();

        $campaign = new Campaign($cache, $db);

        $this->assertFalse($campaign->delete());
        $this->assertNotEmpty($campaign->errors);

        $campaign = new Campaign($cache, $db);

        $campaign->setId(1);

        $this->assertTrue($campaign->delete());
        $this->assertEmpty($campaign->errors);
    }
}
