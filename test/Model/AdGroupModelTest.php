<?php

namespace Test\Model;

use App\Model\AdGroup;
use Test\Model\BaseModelTest;


class AdGroupModelTest extends BaseModelTest
{
    public function adGroupDataProvider()
    {
        $data   = [];
        $dataset = [
            '{"id":1,"name":"Taleggio","status":"OFFLINE","campaign_id":1}',
            '{"id":2,"name":"Emmental","status":"ENABLED","campaign_id":1}',
            '{"id":3,"name":"Parmigiano-Reggiano","status":"OFFLINE","campaign_id":2}'
        ];

        foreach ($dataset as $arg) {
            $data[] = [json_decode($arg, true)];
        }

        return $data;
    }

    public function testGetterSetter()
    {
        list($cache, $db) = $this->mockTestGetterSetter();

        $adGroup = new AdGroup($cache, $db);

        $this->assertNull($adGroup->setId(1));
        $this->assertNull($adGroup->setName('Testing testing'));
        $this->assertNull($adGroup->setStatus('ENABLED'));
        $this->assertNull($adGroup->setStatus('OFFLINE'));
        $this->assertNull($adGroup->setCampaignId(1));
        $this->assertEmpty($adGroup->errors);

        $adGroup = new AdGroup($cache, $db);

        $this->assertFalse($adGroup->setId(null));
        $this->assertFalse($adGroup->setName(null));
        $this->assertFalse($adGroup->setStatus(null));
        $this->assertFalse($adGroup->setCampaignId(null));
        $this->assertNotEmpty($adGroup->errors);
    }

    public function testSelectAll()
    {
        list($cache, $db) = $this->mockTestSelectAll($this->adGroupDataProvider());

        $adGroup = new AdGroup($cache, $db);

        $this->assertNotEmpty($adGroup->select());
    }

    public function testSelect()
    {
        list($cache, $db) = $this->mockTestSelect($this->adGroupDataProvider()[0]);

        $adGroup = new AdGroup($cache, $db);

        $adGroup->setId(1);
        $this->assertNotEmpty($adGroup->select());
    }

    /**
     * @dataProvider adGroupDataProvider
     */
    public function testInsert($data)
    {
        list($cache, $db) = $this->mockTestInsert();
        $adGroup = new AdGroup($cache, $db);

        $this->assertFalse($adGroup->insert());
        $this->assertNotEmpty($adGroup->errors);

        $adGroup = new AdGroup($cache, $db);

        $adGroup->setName($data['name']);
        $adGroup->setStatus($data['status']);
        $adGroup->setCampaignId($data['campaign_id']);

        $this->assertTrue($adGroup->insert());
        $this->assertNotEmpty($adGroup->getId());
        $this->assertEmpty($adGroup->errors);
    }

    /**
     * @dataProvider adGroupDataProvider
     */
    public function testUpdate($data)
    {
        list($cache, $db) = $this->mockTestUpdate();

        $adGroup = new AdGroup($cache, $db);

        $this->assertFalse($adGroup->update());
        $this->assertNotEmpty($adGroup->errors);

        $adGroup = new AdGroup($cache, $db);

        $adGroup->setId($data['id']);

        $this->assertTrue($adGroup->update());
        $this->assertEmpty($adGroup->errors);
    }

    public function testDelete()
    {
        list($cache, $db) = $this->mockTestDelete();

        $adGroup = new AdGroup($cache, $db);

        $this->assertFalse($adGroup->delete());
        $this->assertNotEmpty($adGroup->errors);

        $adGroup = new AdGroup($cache, $db);

        $adGroup->setId(1);

        $this->assertTrue($adGroup->delete());
        $this->assertEmpty($adGroup->errors);
    }
}
