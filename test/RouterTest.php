<?php

namespace Test\Database;

use Lib\Router;
use org\bovigo\vfs\vfsStream;


class RouterTest extends \PHPUnit_Framework_TestCase
{
    protected $preserveGlobalState = false;
    protected $runTestInSeparateProcess = true;

    private $root;

    public function setUp()
    {
        $files = ['Controller' => ['TestController.php' => '']];

        $this->root  = vfsStream::setup('app' , null, $files);

        define('APP_PATH', $this->root->url());
    }

    public function routeDataProvider()
    {
        return [
            ['GET', '/', ['/', 'index@TestController'], true],
            ['GET', '/test', ['/test', 'test@TestController'], true],
            ['PUT', '/', ['/', 'put@TestController'], true],
            ['GET', '/', ['/', 'get@TestController'], true],
            ['GET', '/get', ['/', 'get@TestController'], false],
        ];
    }

    /**
     * @dataProvider routeDataProvider
     */
    public function testFindRoute($method, $uri, $route, $expected)
    {
        $splitUri = explode('/', ltrim($uri, '/'));

        $this->assertEquals($expected, ($splitUri[0] == ltrim($route[0], '/')));
    }

    public function testRegisterRoute()
    {

        $router = new Router();

        $router->register('GET', '/', 'index@TestController');

        $callable = explode('@', 'index@TestController', 2);

        $filePath = str_replace('\\', '/', $callable[1]);
        $filePath = APP_PATH . '/Controller/' . $filePath . '.php';

        $this->assertEquals('App\Controller\TestController', $router->getClassName());
        $this->assertEquals(
            'vfs://'. $this->root->getChild('Controller/TestController.php')->path(),
            $router->getClassPath()
        );

        $this->assertEquals('index@TestController', $router->getRoute());
        $this->assertEquals('index', $router->getClassMethod());

        $router->register('PUT', '/put', 'TestController');

        $this->assertEquals('put@TestController', $router->getRoute());
        $this->assertEquals('put', $router->getClassMethod());
    }
}
