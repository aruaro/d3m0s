<?php

namespace Test\Database;

use Lib\Database\SqliteConnection;


class SqliteConnectionTest extends \PHPUnit_Framework_TestCase
{
    private $db;

    public function setUp()
    {
        $this->db = new \PDO('sqlite::memory:');

        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->db->exec(
            'CREATE TABLE test (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL);'
        );

        $stmt = $this->db->prepare('INSERT INTO test (id, name) VALUES (?, ?);');

        foreach (['Roquefort', 'Camembert', 'Cotija'] as $i => $c) {
            $stmt->bindValue(1, ($i + 1), \PDO::PARAM_INT);
            $stmt->bindValue(2, $c, \PDO::PARAM_STR);
            $stmt->execute();
        }
    }

    public function testConnection()
    {
        $sqlite = new SqliteConnection(['path' => ':memory:']);

        $sqlite->connect();

        $this->assertInstanceOf('\\PDO', $sqlite->getDb());

        $sqlite->disconnect();

        $this->assertNull($sqlite->getDb());
    }

    public function testSelect()
    {
        $sqlite = new SqliteConnection([]);

        $sqlite->setDb($this->db);

        $this->assertNotEmpty($sqlite->query('SELECT * FROM test;'));
    }

    public function testInsert()
    {
        $sqlite = new SqliteConnection([]);

        $sqlite->setDb($this->db);

        $this->assertTrue($sqlite->query('INSERT INTO test (name) VALUES ("insert")'));
        $this->assertNotNull($sqlite->getLastInsertId());
    }

    public function testUpdate()
    {
        $sqlite = new SqliteConnection([]);

        $sqlite->setDb($this->db);

        $this->assertTrue($sqlite->query('UPDATE test SET name = "changed" WHERE id = 1;'));
        $this->assertNotNull($sqlite->getLastInsertId());
        $this->assertEquals(
            [['name' => 'changed']],
            $sqlite->query('SELECT name FROM test WHERE id = 1;')
        );
    }

    public function testDelete()
    {
        $sqlite = new SqliteConnection([]);

        $sqlite->setDb($this->db);

        $this->assertTrue($sqlite->query('DELETE FROM test WHERE id = 3;'));
        $this->assertNotNull($sqlite->getLastInsertId());
        $this->assertEmpty($sqlite->query('SELECT * FROM test WHERE id = 3'));
    }

    public function testQueryExceptions()
    {
        $this->expectException(\PDOException::class);

        $sqlite = new SqliteConnection(['path' => ':memory:']);

        $sqlite->setDb($this->db);
        $sqlite->query('ELECT * FROM test;');
        $sqlite->query('INSERT FROM testtt (name) VALUES ("hey")');
    }
}
