<?php

namespace Test\Cache;

use Lib\Cache\JsonFileCache;
use org\bovigo\vfs\vfsStream;


class JsonFileCacheTest extends \PHPUnit_Framework_TestCase
{
    private $root;
    private $config;
    private $files;

    public function setUp()
    {
        $this->files = [
            'cache' => [
                'exists.*' => md5('{"a": "b"}') . '||{"a": "b"}',
                'exists.1' => md5('{"c": "dd"}') . '||{"c": "d"}',
                'exists.2' => md5('{"a": "b"}') . '||{"a": "b"}'
            ]
        ];

        $this->root = vfsStream::setup('storage', null, $this->files);

        $this->config = [
            'driver'  => 'jsonfile',
            'enabled' => true,
            'expires' => 10,
            'path'    => $this->root->url() . '/cache'
        ];
    }

    public function cacheKeyDataProvider()
    {
        return [
            ['exists.*', ['a' => 'b']],
            ['exists.1', false],
            ['notexists.*', false]
        ];
    }

    /**
     * @dataProvider cacheKeyDataProvider
     */
    public function testfetch($key, $expected)
    {
        $cache = new JsonFileCache($this->config);

        $this->assertEquals($expected, $cache->fetch($key));
    }

    public function testSave()
    {
        $cache = new JsonFileCache($this->config);
        $data  = ['test' => 'test'];

        $this->assertTrue($cache->save('test', $data));
        $this->assertEquals($data, $cache->fetch('test'));
    }

    public function testDelete()
    {
        $cache = new JsonFileCache($this->config);

        $this->assertTrue($cache->delete('exists.2'));
        $this->assertFalse($cache->delete('notexists.1'));
    }

    public function testExpire()
    {
        $this->config['expires'] = 1;

        $cache = new JsonFileCache($this->config);
        $data  = ['test' => 'test'];

        $cache->save('test', $data);

        $this->assertEquals($data, $cache->fetch('test'));

        sleep(1);

        $this->assertFalse($cache->fetch('test'));


        $this->config['expires'] = 2;

        $cache = new JsonFileCache($this->config);
        $data  = ['test2' => 'test2'];

        $cache->save('test2', $data);

        $this->assertEquals($data, $cache->fetch('test2'));

        sleep(1);

        $this->assertEquals($data, $cache->fetch('test2'));
    }
}
