<?php

return [
    ['/', 'index@Api\\HomeController'],
    ['/campaigns', 'Api\\CampaignController'],
    ['/ad_groups', 'Api\\AdGroupController'],
    ['/expanded_text_ads', 'Api\\ExpandedTextAdController'],
];
