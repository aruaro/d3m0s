<?php

namespace App\Model;

use Lib\Model;


class Campaign extends Model
{
    const CACHE_KEY = 'model.campaign';

    const STATUS_PAUSED = 'PAUSED';
    const STATUS_ON     = 'ON';

    const CHANNEL_DISPLAY = 'DISPLAY';
    const CHANNEL_SEARCH  = 'SEARCH';

    private $id;
    private $name;
    private $status;
    private $budget;
    private $advertisingChannelType;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $valid = true;

        if (!is_numeric($id)) {
            $this->errors[] = 'ID must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $valid = true;

        if (!is_string($name)) {
            $this->errors[] = 'Name must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->name = $name;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $valid   = true;
        $allowed = [self::STATUS_ON, self::STATUS_PAUSED];

        if (!in_array($status, $allowed)) {
            $this->errors[] = 'Status must be: ' . implode(' or ', $allowed);
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->status = $status;
    }

    public function getBudget()
    {
        return $this->budget;
    }

    public function setBudget($budget)
    {
        $valid = true;

        if (!is_numeric($budget)) {
            $this->errors[] = 'Budget must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->budget = $budget;
    }

    public function getAdvertisingChannelType()
    {
        return $this->advertisingChannelType;
    }

    public function setAdvertisingChannelType($advertisingChannelType)
    {
        $valid   = true;
        $allowed = [self::CHANNEL_DISPLAY, self::CHANNEL_SEARCH];

        if (!in_array($advertisingChannelType, $allowed)) {
            $this->errors[] = 'Advertising Channel Type must be: ' . implode(' or ', $allowed);
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->advertisingChannelType = $advertisingChannelType;
    }

    public function select()
    {
        $cached = $this->selectCached($this->getId());

        if ($cached !== false) {
            return $cached;
        }

        if ($this->getId() == null) {
            $results = $this->db->query('SELECT * FROM campaign', []);

            $this->cache->save(self::CACHE_KEY . '.*', $results);

            return $results;
        } else {
            $results = $this->db->query('SELECT * FROM campaign WHERE id = ?', [$this->getId()]);

            $this->cache->save(self::CACHE_KEY . '.' . $this->getId(), $results);

            return $results;
        }
    }

    public function insert()
    {
        $required = ['name', 'budget', 'advertisingChannelType'];

        foreach ($required as $r) {
            if (empty($this->$r)) {
                $this->errors[] = $r . ' field is missing.';
            }
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'INSERT INTO campaign (name, status, budget, advertising_channel_type) '
            . 'VALUES(?, ?, ?, ?);';

        $result = $this->db->query($sql, [
            $this->getName(),
            $this->getStatus(),
            $this->getBudget(),
            $this->getAdvertisingChannelType()
        ]);

        $this->setId($this->db->getLastInsertId());

        $this->errors = [];

        return $result;
    }

    public function update()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'UPDATE campaign SET name = ?, status = ?, budget = ?, advertising_channel_type = ? '
            . 'WHERE id = ?;';

        $result = $this->db->query($sql, [
            $this->getName(),
            $this->getStatus(),
            $this->getBudget(),
            $this->getAdvertisingChannelType(),
            $this->getId()
        ]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        return $result;
    }

    public function delete()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $result = $this->db->query('DELETE FROM campaign WHERE id = ?', [$this->getId()]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        return $result;
    }

    private function selectCached($id = null)
    {
        if ($id == null) {
            return $this->cache->fetch(self::CACHE_KEY . '.*');
        } else {
            $cached = $this->cache->fetch(self::CACHE_KEY . '.' . $id);

            if ($cached === false) {
                $cached = $this->cache->fetch(self::CACHE_KEY . '.*');

                if ($cached !== false) {
                    foreach ($cached as $r) {
                        if($r['id'] == $id) {
                            return [$r];
                        }
                    }
                }
            }

            return $cached;
        }
    }
}
