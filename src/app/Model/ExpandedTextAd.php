<?php

namespace App\Model;

use Lib\Model;


class ExpandedTextAd extends Model
{
    const CACHE_KEY = 'model.expandedtextad';

    private $id;
    private $xsiType;
    private $headlinePart1;
    private $headlinePart2;
    private $description;
    private $path1;
    private $path2;
    private $adGroupId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $valid = true;

        if (!is_numeric($id)) {
            $this->errors[] = 'ID must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->id = $id;
    }

    public function getXsiType()
    {
        return $this->xsiType;
    }

    public function setXsiType($xsiType)
    {
        $valid = true;

        if (!is_string($xsiType)) {
            $this->errors[] = 'xsi_type must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->xsiType = $xsiType;
    }

    public function getHeadlinePart1()
    {
        return $this->headlinePart1;
    }

    public function setHeadlinePart1($headlinePart1)
    {
        $valid = true;

        if (!is_string($headlinePart1)) {
            $this->errors[] = 'headline_part1 must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->headlinePart1 = $headlinePart1;
    }

    public function getHeadlinePart2()
    {
        return $this->headlinePart2;
    }

    public function setHeadlinePart2($headlinePart2)
    {
        $valid = true;

        if (!is_string($headlinePart2)) {
            $this->errors[] = 'headline_part2 must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->headlinePart2 = $headlinePart2;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $valid = true;

        if (!is_string($description)) {
            $this->errors[] = 'description must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->description = $description;
    }

    public function getPath1()
    {
        return $this->path1;
    }

    public function setPath1($path1)
    {
        $valid = true;

        if (!is_string($path1)) {
            $this->errors[] = 'path1 must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->path1 = $path1;
    }

    public function getPath2()
    {
        return $this->path2;
    }

    public function setPath2($path2)
    {
        $valid = true;

        if (!is_string($path2)) {
            $this->errors[] = 'path2 must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->path2 = $path2;
    }

    public function getAdGroupId()
    {
        return $this->adGroupId;
    }

    public function setAdGroupId($adGroupId)
    {
        $valid = true;

        if (!is_numeric($adGroupId)) {
            $this->errors[] = 'ad_group_id must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->adGroupId = $adGroupId;
    }

    public function select()
    {
        $cached = $this->selectCached($this->getId());

        if ($cached !== false) {
            return $cached;
        }

        if ($this->getId() == null) {
            $results = $this->db->query('SELECT * FROM expanded_text_ad', []);

            $this->cache->save(self::CACHE_KEY . '.*', $results);

            return $results;
        } else {
            $results = $this->db->query('SELECT * FROM expanded_text_ad WHERE id = ?', [$this->getId()]);

            $this->cache->save(self::CACHE_KEY . '.' . $this->getId(), $results);

            return $results;
        }
    }

    public function insert()
    {
        $required = ['adGroupId', 'headlinePart1'];

        foreach ($required as $r) {
            if (empty($this->$r)) {
                $this->errors[] = $r . ' field is missing.';
            }
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'INSERT INTO expanded_text_ad (xsi_type, ad_group_id, headline_part1, headline_part2, description, path1, path2) '
            .' VALUES(?, ?, ?, ?, ?, ?, ?);';

        $result = $this->db->query($sql, [
            $this->getXsiType(),
            $this->getAdGroupId(),
            $this->getHeadlinePart1(),
            $this->getHeadlinePart2(),
            $this->getDescription(),
            $this->getPath1(),
            $this->getPath2()
        ]);

        $this->setId($this->db->getLastInsertId());

        $this->errors = [];

        return $result;
    }

    public function update()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'UPDATE expanded_text_ad '
            .' SET xsi_type = ?, ad_group_id = ?, headline_part1 = ?, headline_part2 = ?,'
            .' description = ?, path1 = ?, path2 = ?'
            .' WHERE id = ?;';

        $result = $this->db->query($sql, [
            $this->getXsiType(),
            $this->getAdGroupId(),
            $this->getHeadlinePart1(),
            $this->getHeadlinePart2(),
            $this->getDescription(),
            $this->getPath1(),
            $this->getPath2(),
            $this->getId()
        ]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        return $result;
    }

    public function delete()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $result = $this->db->query('DELETE FROM expanded_text_ad WHERE id = ?', [$this->getId()]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        return $result;
    }

    private function selectCached($id = null)
    {
        if ($id == null) {
            return $this->cache->fetch(self::CACHE_KEY . '.*');
        } else {
            $cached = $this->cache->fetch(self::CACHE_KEY . '.' . $id);

            if ($cached === false) {
                $cached = $this->cache->fetch(self::CACHE_KEY . '.*');

                if ($cached !== false) {
                    foreach ($cached as $r) {
                        if($r['id'] == $id) {
                            return [$r];
                        }
                    }
                }
            }

            return $cached;
        }
    }
}
