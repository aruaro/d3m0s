<?php

namespace App\Model;

use Lib\Model;


class AdGroup extends Model
{
    const CACHE_KEY = 'model.adgroup';

    const STATUS_ENABLED = 'ENABLED';
    const STATUS_OFFLINE = 'OFFLINE';

    private $id;
    private $name;
    private $status;
    private $campaignId;

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $valid = true;

        if (!is_numeric($id)) {
            $this->errors[] = 'ID must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $valid = true;

        if (!is_string($name)) {
            $this->errors[] = 'Name must be string';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->name = $name;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $valid   = true;
        $allowed = [self::STATUS_ENABLED, self::STATUS_OFFLINE];

        if (!in_array($status, $allowed)) {
            $this->errors[] = 'Status must be: ' . implode(' or ', $allowed);
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->status = $status;
    }

    public function getCampaignId()
    {
        return $this->campaignId;
    }

    public function setCampaignId($campaignId)
    {
        $valid = true;

        if (!is_numeric($campaignId)) {
            $this->errors[] = 'ID must be numeric';
            $valid = false;
        }

        if ($valid === false) {
            return false;
        }

        $this->campaignId = $campaignId;
    }

    public function select()
    {
        $cached = $this->selectCached($this->getId());

        if ($cached !== false) {
            return $cached;
        }

        if ($this->getId() == null) {
            $results = $this->db->query('SELECT * FROM ad_group', []);

            $this->cache->save(self::CACHE_KEY . '.*', $results);

            return $results;
        } else {
            $results = $this->db->query('SELECT * FROM ad_group WHERE id = ?', [$this->getId()]);

            $this->cache->save(self::CACHE_KEY . '.' . $this->getId(), $results);

            return $results;
        }
    }

    public function insert()
    {
        $required = ['name', 'status', 'campaignId'];

        foreach ($required as $r) {
            if (empty($this->$r)) {
                $this->errors[] = $r . ' field is missing.';
            }
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'INSERT INTO ad_group (name, status, campaign_id) VALUES(?, ?, ?);';

        $result = $this->db->query($sql, [
            $this->getName(),
            $this->getStatus(),
            $this->getCampaignId()
        ]);

        $this->setId($this->db->getLastInsertId());

        $this->errors = [];

        return $result;
    }

    public function update()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $sql = 'UPDATE ad_group SET name = ?, status = ?, campaign_id = ? WHERE id = ?;';

        $result = $this->db->query($sql, [
            $this->getName(),
            $this->getStatus(),
            $this->getCampaignId(),
            $this->getId()
        ]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        return $result;
    }

    public function delete()
    {
        if (empty($this->getId())) {
            $this->errors[] = 'ID required';
        }

        if(!empty($this->errors)) {
            return false;
        }

        $result = $this->db->query('DELETE FROM ad_group WHERE id = ?', [$this->getId()]);

        $this->cache->delete(self::CACHE_KEY . '.' . $this->getId());

        var_dump($this->db->getLastInsertId());

        return $result;
    }

    private function selectCached($id = null)
    {
        if ($id == null) {
            return $this->cache->fetch(self::CACHE_KEY . '.*');
        } else {
            $cached = $this->cache->fetch(self::CACHE_KEY . '.' . $id);

            if ($cached === false) {
                $cached = $this->cache->fetch(self::CACHE_KEY . '.*');

                if ($cached !== false) {
                    foreach ($cached as $r) {
                        if($r['id'] == $id) {
                            return [$r];
                        }
                    }
                }
            }

            return $cached;
        }
    }
}
