<?php

namespace App\Controller\Api;

use Lib\Http\Response;
use App\Model\ExpandedTextAd;


class ExpandedTextAdController extends RestApiController
{
    /**
     * View data and show them in JSON format
     * Cache data if requested again
     * @return Http\Response
     */
    public function get()
    {
        $id = count($this->rq->getSplitUri()) > 1 ? $this->rq->getSplitUri()[1] : null;

        $textAd = new ExpandedTextAd($this->s['cache'], $this->s['database']);

        if (!empty($id)) {
            $textAd->setId($id);
        }

        return $this->rs->outputJson($textAd->select());
    }

    /**
     * Add new record
     * @return Http\Response
     */
    public function post()
    {
        $textAd = new ExpandedTextAd($this->s['cache'], $this->s['database']);
        $data   = $this->rq->getBody('post');

        $textAd->setXsiType($data['xsi_type']);
        $textAd->setHeadlinePart1($data['headline_part1']);
        $textAd->setHeadlinePart2($data['headline_part2']);
        $textAd->setDescription($data['description']);
        $textAd->setPath1($data['path1']);
        $textAd->setPath2($data['path2']);
        $textAd->setAdGroupId($data['ad_group_id']);

        if ($textAd->insert() === false) {
            return $this->rs->outputJson(['errors' => $textAd->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($textAd->select());
        }
    }

    /**
     * Update the record
     * @return Http\Response
     */
    public function put()
    {
        $textAd = new ExpandedTextAd($this->s['cache'], $this->s['database']);
        $data   = $this->rq->getBody('put');

        $textAd->setId($data['id']);

        if (empty($origData = $textAd->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($textAd->getId() != null) {
            $data = array_merge($origData, $data);

            $textAd->setXsiType($data['xsi_type']);
            $textAd->setHeadlinePart1($data['headline_part1']);
            $textAd->setHeadlinePart2($data['headline_part2']);
            $textAd->setDescription($data['description']);
            $textAd->setPath1($data['path1']);
            $textAd->setPath2($data['path2']);
            $textAd->setAdGroupId($data['ad_group_id']);
        }

        if ($textAd->update() === false) {
            return $this->rs->outputJson(['errors' => $textAd->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($textAd->select());
        }
    }

    /**
     * Delete the record
     * @return Http\Response
     */
    public function delete()
    {
        $textAd = new ExpandedTextAd($this->s['cache'], $this->s['database']);
        $id       = $this->rq->getSplitUri()[1];

        if (!empty($id)) {
            $textAd->setId($id);
        }

        if (empty($data = $textAd->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($textAd->delete() === false) {
            return $this->rs->outputJson(['errors' => $textAd->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson(['message' => '#' . $id  . ' deleted', 'data' => $data]);
        }
    }
}
