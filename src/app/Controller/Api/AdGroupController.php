<?php

namespace App\Controller\Api;

use Lib\Http\Response;
use App\Model\AdGroup;


class AdGroupController extends RestApiController
{
    /**
     * View data and show them in JSON format
     * Cache data if requested again
     * @return Http\Response
     */
    public function get()
    {
        $id = count($this->rq->getSplitUri()) > 1 ? $this->rq->getSplitUri()[1] : null;

        $adGroup = new AdGroup($this->s['cache'], $this->s['database']);

        if (!empty($id)) {
            $adGroup->setId($id);
        }

        return $this->rs->outputJson($adGroup->select());
    }

    /**
     * Add new record
     * @return Http\Response
     */
    public function post()
    {
        $adGroup = new AdGroup($this->s['cache'], $this->s['database']);
        $data     = $this->rq->getBody('post');

        $adGroup->setName($data['name']);
        $adGroup->setStatus($data['status']);
        $adGroup->setCampaignId($data['campaign_id']);

        if ($adGroup->insert() === false) {
            return $this->rs->outputJson(['errors' => $adGroup->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($adGroup->select());
        }
    }

    /**
     * Update the record
     * @return Http\Response
     */
    public function put()
    {
        $adGroup = new AdGroup($this->s['cache'], $this->s['database']);
        $data     = $this->rq->getBody('put');

        $adGroup->setId($data['id']);

        if (empty($origData = $adGroup->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($adGroup->getId() != null) {
            $data = array_merge($origData, $data);

            $adGroup->setName($data['name']);
            $adGroup->setStatus($data['status']);
            $adGroup->setCampaignId($data['campaign_id']);
        }

        if ($adGroup->update() === false) {
            return $this->rs->outputJson(['errors' => $adGroup->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($adGroup->select());
        }
    }

    /**
     * Delete the record
     * @return Http\Response
     */
    public function delete()
    {
        $adGroup = new AdGroup($this->s['cache'], $this->s['database']);
        $id      = $this->rq->getSplitUri()[1];

        if (!empty($id)) {
            $adGroup->setId($id);
        }

        if (empty($data = $adGroup->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($adGroup->delete() === false) {
            return $this->rs->outputJson(['errors' => $adGroup->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson(['message' => '#' . $id  . ' deleted', 'data' => $data]);
        }
    }
}
