<?php

namespace App\Controller\Api;

use Lib\Container;
use Lib\ControllerInterface;


class RestApiController implements ControllerInterface
{
    protected $rq;
    protected $rs;
    protected $c;
    protected $s;

    public function __construct($request, $response, $config, $services)
    {
        $this->rq = $request;
        $this->rs = $response;
        $this->c  = $config;
        $this->s  = $services;
    }
}
