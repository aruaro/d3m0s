<?php

namespace App\Controller\Api;

use Lib\Http\Response;
use App\Model\Campaign;


class CampaignController extends RestApiController
{
    /**
     * View data and show them in JSON format
     * Cache data if requested again
     * @return Http\Response
     */
    public function get()
    {
        $id = count($this->rq->getSplitUri()) > 1 ? $this->rq->getSplitUri()[1] : null;

        $campaign = new Campaign($this->s['cache'], $this->s['database']);

        if (!empty($id)) {
            $campaign->setId($id);
        }

        return $this->rs->outputJson($campaign->select());
    }

    /**
     * Add new record
     * @return Http\Response
     */
    public function post()
    {
        $campaign = new Campaign($this->s['cache'], $this->s['database']);
        $data     = $this->rq->getBody('post');

        $campaign->setName($data['name']);
        $campaign->setStatus($data['status']);
        $campaign->setBudget($data['budget']);
        $campaign->setAdvertisingChannelType($data['advertising_channel_type']);

        if ($campaign->insert() === false) {
            return $this->rs->outputJson(['errors' => $campaign->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($campaign->select());
        }
    }

    /**
     * Update the record
     * @return Http\Response
     */
    public function put()
    {
        $campaign = new Campaign($this->s['cache'], $this->s['database']);
        $data     = $this->rq->getBody('put');

        $campaign->setId($data['id']);

        if (empty($origData = $campaign->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($campaign->getId() != null) {
            $data = array_merge($origData, $data);

            $campaign->setName($data['name']);
            $campaign->setStatus($data['status']);
            $campaign->setBudget($data['budget']);
            $campaign->setAdvertisingChannelType($data['advertising_channel_type']);
        }

        if ($campaign->update() === false) {
            return $this->rs->outputJson(['errors' => $campaign->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson($campaign->select());
        }
    }

    /**
     * Delete the record
     * @return Http\Response
     */
    public function delete()
    {
        $campaign = new Campaign($this->s['cache'], $this->s['database']);
        $id       = $this->rq->getSplitUri()[1];

        if (!empty($id)) {
            $campaign->setId($id);
        }

        if (empty($data = $campaign->select())) {
            return $this->rs->outputJson(['errors' => 'Object does not exist'], Response::CODE_BADREQUEST);
        }

        if ($campaign->delete() === false) {
            return $this->rs->outputJson(['errors' => $campaign->errors], Response::CODE_ERROR);
        } else {
            return $this->rs->outputJson(['message' => '#' . $id  . ' deleted', 'data' => $data]);
        }
    }
}
