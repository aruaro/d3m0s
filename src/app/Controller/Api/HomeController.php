<?php

namespace App\Controller\Api;

use App\Model\Campaign;


class HomeController
{
    private $config;
    private $req;
    private $res;

    public function __construct($request, $response, $config, $services)
    {
        $this->req      = $request;
        $this->res      = $response;
        $this->config   = $config;
    }

    public function index()
    {
        return $this->res->outputJson('hello');
    }
}
