<?php

namespace Lib\Cache;


class CacheServiceProvider
{
    public static function register($config)
    {
        $cache = null;

        switch ($config['driver']) {
            case 'jsonfile':
                $cache = new JsonFileCache($config);
                break;
            default:
                throw new \Exception('No cache service configured', 500);
        }

        return $cache;
    }
}
