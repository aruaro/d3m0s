<?php

namespace Lib\Cache;


interface CacheInterface
{
    public function fetch($key);

    public function save($key, $data);

    public function delete($key);
}
