<?php

namespace Lib\Cache;

use Lib\Cache\Cache;


class JsonFileCache extends Cache
{
    private $cachePath;
    private $enabled;
    private $expires; // seconds

    public function __construct($config)
    {
        $this->cachePath = $config['path'];
        $this->expires   = $config['expires'];
        $this->enabled   = $config['enabled'];
    }

    public function getKeyPath($key)
    {
        return $this->cachePath . '/' . $key;
    }

    public function isKeyExist($key)
    {
        return file_exists($this->getKeyPath($key));
    }

    public function fetch($key)
    {
        if ($this->enabled === false || $this->isKeyExist($key) === false) {
            return false;
        }

        $cacheFile = $this->getKeyPath($key);

        // cache still valid
        if (file_exists($cacheFile) && (filemtime($cacheFile) > (time() - $this->expires))) {
            $content = @file_get_contents($cacheFile);
            $content = explode('||', $content, 2);

            // file has been tampered with
            if (md5($content[1]) != $content[0]) {
                return false;
            }

            return json_decode($content[1], true);
        }

        return false;
    }

    public function save($key, $data)
    {
        if ($this->enabled === false) {
            return false;
        }
        // simple method of content hashing to prevent tampering of data
        @file_put_contents(
            $this->getKeyPath($key),
            md5(json_encode($data)) . '||' . json_encode($data)
        );

        return true;
    }

    public function delete($key)
    {
        if ($this->enabled === false) {
            return false;
        }

        if ($this->isKeyExist($key)) {
            @unlink($this->getKeyPath($key));
            return true;
        }

        return false;
    }
}
