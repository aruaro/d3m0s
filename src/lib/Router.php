<?php

namespace Lib;

use Lib\Container;
use Lib\Exception\RouteNotFoundException;


class Router
{
    private $route;
    private $classPath;
    private $className;
    private $classMethod;

    public function getClassPath()
    {
        return $this->classPath;
    }

    public function getClassName()
    {
        return $this->className;
    }

    public function getClassMethod()
    {
        return $this->classMethod;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function callRoute($request, $response, $config, $services)
    {
        $calledClass  = $this->getClassName();
        $calledMethod = $this->getClassMethod();

        if(class_exists($calledClass)) {
            if(method_exists($calledClass, $calledMethod)) {
                try {
                    $calledObj = new $calledClass($request, $response, $config, $services);

                    return $calledObj->$calledMethod();
                } catch(\Exception $e) {
                    throw new RouteNotFoundException($e->getMessage(), $this->getRoute());
                }
            } else {
                throw new RouteNotFoundException('Method not found', $this->getRoute());
            }
        } else {
            throw new RouteNotFoundException('Class not found', $this->getRoute());
        }
    }

    public function findRoute($routes, $request)
    {
        foreach ($routes as $route) {
            // one-level URI
            if ($request->getSplitUri()[0] == ltrim($route[0], '/')) {
                $this->register($request->getMethod(), $route[0], $route[1]);
                break;
            }
        }

        if (empty($this->getRoute())) {
            throw new RouteNotFoundException('Route not found', $request->getUri());
        }
    }

    public function register($method, $name, $path)
    {
        // assume RESTful API by appending method to controller class
        if (strpos($path, '@') === false) {
            $path = strtolower($method) . '@' . $path;
        }

        $callable = explode('@', $path, 2);

        $filePath = str_replace('\\', '/', $callable[1]);
        $filePath = APP_PATH . '/Controller/' . $filePath . '.php';

        if (file_exists($filePath)) {
            $this->route       = $path;
            $this->classPath   = $filePath;
            $this->className   = 'App\\Controller\\' . $callable[1];
            $this->classMethod = $callable[0];
        } else {
            throw new RouteNotFoundException($name . ' : ' . $path, 404);
        }

        return true;
    }
}
