<?php

namespace Lib;


interface ModelInterface
{
    public function get();

    public function add();

    public function update();

    public function delete();
}
