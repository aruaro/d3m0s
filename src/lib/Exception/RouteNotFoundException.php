<?php

namespace Lib\Exception;


class RouteNotFoundException extends \Exception
{
    public function __construct($message, $route, $code = 0, \Exception $previous = null) {
        $message = $route . ' -- ' . $message;
        $code    = 404;

        parent::__construct($message, $code, $previous);
    }
}
