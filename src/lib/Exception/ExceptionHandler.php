<?php

namespace Lib\Exception;

class ExceptionHandler
{
    public function __construct()
    {
        @set_exception_handler([$this, 'handle']);
    }

    public function handle($e)
    {
        $error = [
            'code'    => $e->getCode(),
            'message' => $e->getMessage()
        ];

        if (APP_ENV == 'dev') {
            $error['file'] = $e->getFile();
            $error['line'] = $e->getLine();
        }

        http_response_code($e->getCode());
        header('Content-Type: application/json');

        echo json_encode($error);
    }
}
