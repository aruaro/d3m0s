<?php

namespace Lib\Http;

use Lib\Http\RequestInterface;
use Lib\Http\Response;
use Lib\Exception\AppException;


class Request implements RequestInterface
{
    private $method;
    private $headers;
    private $uri;
    private $body;

    public function __construct()
    {
        $this->headers = [];
        $this->body    = [];

        $this->parseHeaders();
        $this->parseMethod();
        $this->parseUri();
        $this->parseBody();
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getHeader($key = null)
    {
        if($key != null && isset($this->headers[$key])) {
            return $this->headers[$key];
        }

        return $this->headers;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getSplitUri()
    {
        return explode('/', ltrim($this->uri, '/'));
    }

    public function getBody($key = null)
    {
        if ($key != null && isset($this->body[$key])) {
            return $this->body[$key];
        }

        return $this->body;
    }

    private function parseHeaders()
    {
        foreach ($_SERVER as $k => $v) {
            if (strpos($k, 'HTTP_') === false) {
                continue;
            }

            $k = str_replace('HTTP_', '', $k);
            $k = strtolower($k);

            $this->headers[$k] = $v;
        }
    }

    private function parseMethod()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
    }

    private function parseUri()
    {
        $this->uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
        // $this->uri = str_replace('index.php', '', $this->uri);
        // $this->uri = str_replace('//', '/', $this->uri);
    }

    private function parseBody()
    {
        if ($this->method == 'GET') {
            $this->body['get'] = $_GET;
        } else {
            $stream = fopen('php://input', 'r');
            $body   = '';

            while ($data = fread($stream, 1024)) {
                $body .= $data;
            }

            fclose($stream);

            switch ($this->headers['content_type']) {
                case 'application/json':
                default:
                    $body = json_decode($body, true);

                    if (strtolower(json_last_error_msg()) != 'no error') {
                        throw new \Exception(
                            'JSON request: ' . json_last_error_msg(),
                            Response::CODE_BADREQUEST
                        );
                    }
            }

            if ($this->method == 'POST') {
                $this->body['post'] = $body;
            } elseif ($this->method == 'PUT') {
                $this->body['put'] = $body;
            }
        }
    }
}
