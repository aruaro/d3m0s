<?php

namespace Lib\Http;

use Lib\Http\ResponseInterface;
use Lib\Http\Output\JsonOutput;


class Response implements ResponseInterface
{
    const CODE_OK           = 200;
    const CODE_BADREQUEST   = 400;
    const CODE_UNAUTHORIZED = 401;
    const CODE_FORBIDDEN    = 403;
    const CODE_NOTFOUND     = 404;
    const CODE_ERROR        = 500;

    private $headers;
    private $code;
    private $body;

    public function __construct()
    {
        $this->headers = [];
        $this->code    = 0;
        $this->body    = '';
    }

    public function __call($method, $args)
    {
        $renderer = null;

        if (strpos($method, 'output') === 0) {
            switch ($method) {
                case 'outputJson':
                default:
                    $this->setHeader('Content-Type', 'text/json');

                    $renderer = new JsonOutput();
            }

            $this->setBody($renderer->render($args[0]));

            if (isset($args[1]) && is_numeric($args[1])) {
                $this->setCode($args[1]);
            }

            return $this;
        }
    }

    public function setHeader($key, $param)
    {
        $this->headers[$key] = $param;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function setBody($body)
    {
        $this->body = $body;
    }

    public function send()
    {
        foreach ($this->headers as $k => $v) {
            header($k . ':' . $v);
        }

        http_response_code($this->code);

        ob_start();

        echo $this->body;

        ob_end_flush();
    }
}
