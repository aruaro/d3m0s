<?php

namespace Lib\Http\Output;


interface OutputInterface
{
    public function render($args);
}
