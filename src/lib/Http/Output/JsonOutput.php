<?php

namespace Lib\Http\Output;

use Lib\Http\Output\OutputInterface;


class JsonOutput implements OutputInterface
{
    public function render($args)
    {
        return json_encode($args);
    }
}
