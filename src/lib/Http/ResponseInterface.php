<?php

namespace Lib\Http;


interface ResponseInterface
{
    public function setHeader($key, $param);

    public function setCode($code);

    public function setBody($body);
}
