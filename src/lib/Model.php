<?php

namespace Lib;

use Lib\ModelInterface;


class Model implements ModelInterface
{
    protected $db;
    protected $cache;

    public $errors;

    public function __construct($cache, $db)
    {
        $this->cache = $cache;
        $this->db    = $db;

        $this->errors = [];
    }

    public function get()
    {

    }

    public function add()
    {

    }

    public function update()
    {

    }

    public function delete()
    {

    }

    public function validate()
    {

    }
}
