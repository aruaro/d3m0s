<?php

namespace Lib\Database;


interface ConnectionInterface
{
    public function connect();

    public function disconnect();

    public function query($query, $params);
}
