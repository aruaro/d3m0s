<?php

namespace Lib\Database;

use Lib\Database\ConnectionInterface;


class Connection implements ConnectionInterface
{
    protected $db;
    protected $config;

    protected $lastInsertId;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getDb()
    {
        return $this->db;
    }

    public function setDb($db)
    {
        $this->db = $db;
    }

    public function connect()
    {

    }

    public function disconnect()
    {

    }

    public function query($query, $params)
    {

    }

    public function getLastInsertId()
    {
        return $this->lastInsertId;
    }
}
