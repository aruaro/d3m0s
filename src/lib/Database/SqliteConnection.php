<?php

namespace Lib\Database;

use Lib\Database\Connection;


class SqliteConnection extends Connection
{
    public function connect()
    {
        if($this->getDb() != null) {
            return true;
        }

        $this->db = new \PDO('sqlite:' . $this->config['path']);

        $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    public function disconnect()
    {
        $this->db     = null;
        $this->config = null;
    }

    public function query($sql, $params = [])
    {
        $stmt = $this->db->prepare($sql);
        $i    = 1;

        foreach ($params as $p) {
            $stmt->bindValue($i++, $p, \PDO::PARAM_STR);
        }

        $stmt->execute();

        $this->lastInsertId = $this->db->lastInsertId();

        if (strpos($sql, 'SELECT') === 0) {
            return $stmt->fetchAll(\PDO::FETCH_ASSOC);
        } else {
            return true;
        }
    }
}