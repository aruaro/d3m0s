<?php

namespace Lib\Database;


class DatabaseServiceProvider
{
    public static function register($config)
    {
        $db = null;

        switch ($config['driver']) {
            case 'sqlite':
                $db = new SqliteConnection($config);
                break;
            default:
                throw new \Exception('No database service configured', 500);
        }

        $db->connect();

        return $db;
    }
}
