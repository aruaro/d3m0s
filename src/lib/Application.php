<?php

namespace Lib;

use Lib\Container;
use Lib\Router;
use Lib\Http\Request;
use Lib\Http\Response;
use Lib\Exception\ExceptionHandler;


class Application
{
    public function __construct($config, $routes, $services)
    {
        // set JSON exceptions first
        $exceptionHandler = new ExceptionHandler();

        $router   = new Router();
        $request  = new Request();
        $response = new Response();
        $services = $this->registerServices($services, $config);

        $router->findRoute($routes, $request);

        if(!empty($router->getRoute())) {
            $_response = $router->callRoute($request, $response, $config, $services);

            if($_response != null) {
                $response = $_response;
            }
        }

        $response->send();
    }

    private function registerServices($services, $config)
    {
        $registeredServices = [];

        foreach ($services as $name => $s) {
            $registeredServices[$name] = $s::register($config[$name]);
        }

        return $registeredServices;
    }
}
