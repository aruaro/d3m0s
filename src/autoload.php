<?php
// class autoloader
spl_autoload_register(function ($className) {
    $filePath = str_replace('\\', '/', lcfirst($className));
    $filePath = __DIR__ . '/' . $filePath . '.php';

    if (file_exists($filePath)) {
        require_once $filePath;
        return true;
    } else {
        // throw new \Exception('Class ' . $fileName . ' does not exist.', 500);
    }

    return false;
});
