BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `campaign` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `name` TEXT NOT NULL,
    `status` TEXT NOT NULL,
    `budget` INTEGER DEFAULT 0,
    `advertising_channel_type` TEXT NOT NULL
);
CREATE TABLE IF NOT EXISTS `ad_group` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `name` TEXT NOT NULL,
    `status` TEXT NOT NULL,
    `campaign_id` INTEGER NOT NULL,
    FOREIGN KEY(`campaign_id`) REFERENCES campaign(id)
);
CREATE TABLE IF NOT EXISTS `expanded_text_ad` (
    `id` INTEGER PRIMARY KEY AUTOINCREMENT,
    `xsi_type` TEXT NOT NULL DEFAULT "ExpandedTextAd",
    `ad_group_id` INTEGER NOT NULL,
    `headline_part1` TEXT,
    `headline_part2` TEXT,
    `description` TEXT,
    `path1` TEXT,
    `path2` TEXT,
    FOREIGN KEY(`ad_group_id`) REFERENCES ad_group(id)
);
INSERT INTO campaign (name, status, budget, advertising_channel_type)
VALUES
 ('Interplanetary Cruise', 'PAUSED', 3000, 'SEARCH'),
 ('Police Station', 'ON', 2000, 'DISPLAY');

 INSERT INTO ad_group (name, status, campaign_id)
 VALUES
 ('Facebook Ads - Napice', 'ENABLED', 1),
 ('Google Ads - Napice', 'ENABLED', 1),
 ('Yahoo Ads - Spotify', 'OFFLINE', 1);

 INSERT INTO expanded_text_ad (xsi_type, ad_group_id, headline_part1, headline_part2, description, path1, path2)
 VALUES
    ('ExpandedTextAd', 1, 'Developers from Mars', '', 'Buy your tickets now!', 'all-inclusive', 'deals'),
    ('ExpandedTextAd', 3, 'We are looking for the best designers', 'Best in the galaxy', 'We are a team of product builders and are looking for designers', 'all-inclusive', 'deals');
COMMIT;
